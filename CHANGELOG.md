# bundler-audit analyzer changelog

## v2.0.0
- Switch to new report syntax with `version` field

## v1.1.0
- Add dependency (package name and version) to report
- Improve compare key, remove vulnerability name

## v1.0.0
- Initial release
