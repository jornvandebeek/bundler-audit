# bundler-audit analyzer

Dependency Scanning for Ruby projects. It's based on [bunlder-audit](https://github.com/rubysec/bundler-audit).

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.
